db.getCollection("usuario").insertMany([{"Nombres": "Luis", "Apellidos": "Velasco", "Correo": "kryrox@gmail.com", "Contrase�a": "123456a",
                                        "Direccion": "Av Blango Galindo", "TarjetaDebito": 453216578541, "FechaRegistro": new Date()},
                                        {"Nombres": "Juan", "Apellidos": "Perez Ortiz", "Correo": "jperez@gmail.com", "Contrase�a": "asd321",
                                        "Direccion": "Av America", "TarjetaDebito": 635412784516, "FechaRegistro": new Date(2022,1,30)},
                                        {"Nombres": "Maria", "Apellidos": "Zurita Bola�os", "Correo": "BM123@gmail.com", "Contrase�a": "8dsa74",
                                        "Direccion": "Av Recoleta", "TarjetaDebito": 753165425845, "FechaRegistro": new Date(2020,9,11)},
                                        {"Nombres": "Sarah Kim", "Apellidos": "Bock Yoo", "Correo": "sarahkby@gmail.com", "Contrase�a": "98d7sa",
                                        "Direccion": "Av Los Olivos", "TarjetaDebito": 453216578541, "FechaRegistro": new Date(2010,9,2)},
                                        {"Nombres": "Jos� Maria", "Apellidos": "Rodrigo Herbas", "Correo": "herbasR@gmail.com", "Contrase�a": "98c4fsad",
                                        "Direccion": "Av Villazon", "TarjetaDebito": 635412754623, "FechaRegistro": new Date(2012,6,17)},])


var idLuis = ObjectId("62a9e9246f03248e8a6d9537")
var idJuan = ObjectId("62a9e9246f03248e8a6d9538")
var idMaria = ObjectId("62a9e9246f03248e8a6d9539")
var idSarah = ObjectId("62a9e9246f03248e8a6d953a")
var idJos� = ObjectId("62a9e9246f03248e8a6d953b")


db.getCollection("compra").insertMany([{"Articulo": "Memoria RAM", "Cantidad": 2, "Monto": "84 $", "FechaCompra": new Date(),
                                       "usuarioID": idLuis},
                                       {"Articulo": "Placa Madre", "Cantidad": 5, "Monto": "425 $", "FechaCompra": new Date(2022,6,25),
                                        "usuarioID": idJuan},
                                       {"Articulo": "Audifonos", "Cantidad": 1, "Monto": "152.11 $", "FechaCompra": new Date(),
                                         "usuarioID": idMaria},
                                       {"Articulo": "Tarjeta Grafica", "Cantidad": 2, "Monto": "1600 $", "FechaCompra": new Date(2016,6,10),
                                          "usuarioID": idSarah},
                                       {"Articulo": "Cooler", "Cantidad": 4, "Monto": "80 $", "FechaCompra": new Date(2020,3,31),
                                          "usuarioID": idJos�}])

db.getCollection("usuariocuenta").insertMany([{"Nombres": "Luis", "Apellidos": "Velasco", "Correo": "kryrox@gmail.com", 
                                                "EstadoCuenta": "Activo", "FechaRegistro": new Date()},
                                              {"Nombres": "Juan","Apellidos": "Perez Ortiz", "Correo": "jperez@gmail.com", 
                                                  "EstadoCuenta": "Activo", "FechaRegistro": new Date(2022,1,30)},
                                              {"Nombres": "Maria", "Apellidos": "Zurita Bola�os", "Correo": "BM123@gmail.com", 
                                                  "EstadoCuenta": "Activo", "FechaRegistro": new Date(2020,9,11)},
                                              {"Nombres": "Sarah Kim", "Apellidos": "Bock Yoo", "Correo": "sarahkby@gmail.com", 
                                                  "EstadoCuenta": "Inactivo", "FechaRegistro": new Date(2010,9,2)},
                                              {"Nombres": "Jos� Maria", "Apellidos": "Rodrigo Herbas", "Correo": "herbasR@gmail.com", 
                                                  "EstadoCuenta": "Inactivo", "FechaRegistro": new Date(2012,6,17)}])
                                
db.getCollection("usuario").createIndex([{"Correo": 1}, {unique: true}])
db.getCollection("usuario").createIndex([{"tarjetaDebito": 1}])
db.getCollection("usuario").getIndexes()

db.getCollection("compra").createIndex([{"fechaCompra": 1}])
db.getCollection("compra").getIndexes()

db.getCollection("usuarioCuenta").createIndex([{"estadoCuenta": 1}])
db.getCollection("usuarioCuenta").getIndexes()

db.getCollection("compra").find({"usuarioID":idSarah})
                                           