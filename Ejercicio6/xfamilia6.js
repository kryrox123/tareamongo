db.getCollection("xfamilia").insertMany([{"nombre": "Francisco", "roll": "padre", "edad": 47},
                                         {"nombre": "Alejandra", "roll": "madre", "edad": 42},
                                         {"nombre": "Antonio", "roll": "hijo mayor", "edad": 20},
                                         {"nombre": "Agustina", "roll": "hija menor", "edad": 15},
                                         {"nombre": "Fanny", "roll": "abuela", "edad": 70}])
                                         

db.getCollection("xfamilia").createIndex({"nombre": 1}, {background: true})
db.getCollection("xfamilia").createIndex({"edad": 3}, {background: true})