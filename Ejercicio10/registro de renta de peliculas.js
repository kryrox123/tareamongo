db.getCollection("renta_peli").insertMany([{"nombre_cliente": "Pablo", "nombre_pelicula": "Rapidos y furiosos", "fecha_de_renta": new Date()},
                                           {"nombre_cliente": "Doris", "nombre_pelicula": "La cenicienta", "fecha_de_renta": new Date(2021,2,12)},
                                           {"nombre_cliente": "Jorge", "nombre_pelicula": "El se�or de los anillos", "fecha_de_renta": new Date(2022,1,1)},
                                           {"nombre_cliente": "Boris", "nombre_pelicula": "Hombres de negro", "fecha_de_renta": new Date(2018,3,12)},
                                           {"nombre_cliente": "Luna", "nombre_pelicula": "Warcraft", "fecha_de_renta": new Date(2016,4,6)},
                                           {"nombre_cliente": "Mauricio", "nombre_pelicula": "Scooby Doo", "fecha_de_renta": new Date(2020,2,28)},
                                           {"nombre_cliente": "Laura", "nombre_pelicula": "El Hobbit", "fecha_de_renta": new Date(2017,11,12)},
                                           {"nombre_cliente": "Andres", "nombre_pelicula": "Transformers", "fecha_de_renta": new Date(2012,5,5)}])
                                           
db.getCollection("renta_peli").find({})

db.getCollection("renta_peli").createIndex({"nombre_cliente": 2})
db.getCollection("renta_peli").createIndex({"nombre_pelicula": 4})

db.getCollection("renta_peli").getIndexes()