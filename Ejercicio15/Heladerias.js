db.getCollection("empleado").insertMany([{"nombre":"Luis","apellido":"Gomez","ci":5210321,"estado":"casado","correo":"pegaso@gmail.com","fecha_de_insercion":new Date(2015,6,1)},

                                        {"nombre":"Matias","apellido":"Perez","ci":5632123,"estado":"casado","correo":"xyz@gmail.com","fecha_de_insercion":new Date(2015,11,11)},

                                        {"nombre":"Carlos","apellido":"Villagran","ci":5225412,"estado":"soltero","correo":"asdasd123@gmail.com","fecha_de_insercion":new Date(2012,0,31)},

                                        {"nombre":"Angel","apellido":"Figueroa","ci":6632541,"estado":"soltero","correo":"asd@gmail.com","fecha_de_insercion":new Date(2015,4,4)},

                                        {"nombre":"Domingo","apellido":"Gamon","ci":88457412,"estado":"casado","correo":"help123@gmail.com","fecha_de_insercion":new Date()}])



var idLuis= ObjectId("62b3f535f6c72b4c2070fd23")

var idMatias = ObjectId("62b3f535f6c72b4c2070fd24")

var idCarlos = ObjectId("62b3f535f6c72b4c2070fd25")

var idAngel = ObjectId("62b3f535f6c72b4c2070fd26")

var idDomingo = ObjectId("62b3f535f6c72b4c2070fd27")



db.getCollection("cliente").insertMany([{"nombre":"Alejandro","apellido":"Fernandez","ci":4458541,"Frecuencia_de_compra":2,"correoCliente":"abduzcan@gmail.com"},

                                        {"nombre":"Brian","apellido":"Medina","ci":4412541,"Frecuencia_de_compra":3,"correoCliente":"copali@gmail.com"},

                                        {"nombre":"Victor","apellido":"Apaz","ci":5523641,"Frecuencia_de_compra":5,"correoCliente":"ellexvh@gmail.com"},

                                        {"nombre":"Leandro","apellido":"Zubieta","ci":0000,"Frecuencia_de_compra":1,"correoCliente":"kilwqeian@gmail.com"},

                                        {"nombre":"Adriana","apellido":"Ortiz","ci":4412102,"Frecuencia_de_compra":10,"correoCliente":"rip532@gmail.com"}])



var idAlejandro = ObjectId("62b3f6b8f6c72b4c2070fd28")

var idBrian = ObjectId("62b3f6b8f6c72b4c2070fd29")

var idVictor = ObjectId("62b3f6b8f6c72b4c2070fd2a")

var idLeandro= ObjectId("62b3f6b8f6c72b4c2070fd2b")

var idAdriana = ObjectId("62b3f6b8f6c72b4c2070fd2c")



db.getCollection("heladeria").insertMany([{"nombre":"Dumbo","NIT":1111,"direccion":"Av. Heroinas","IdEmpleado": idLuis ,"idCliente":idAlejandro},

                                           {"nombre":"Donal","NIT":2222,"direccion":"Av. Heroinas","IdEmpleado": idMatias,"idCliente":idBrian},

                                            {"nombre":"dumbo","NIT":3333,"direccion":"Av. heroinas","IdEmpleado":idCarlos,"idCliente":idVictor},

                                            {"nombre":"case","NIT":4444,"direccion":"Av. oquendo","IdEmpleado": idAngel,"idCliente":idLeandro},

                                            {"nombre":"uniti","NIT":5555,"direccion":"Av. america","IdEmpleado":idDomingo,"idCliente":idAdriana},

                                           ])



db.getCollection("pago").insertMany([

{"monto":100,"Tipo_de_pago":"credito","fecha_de_pago":new Date(),"idEmpleado": idLuis},

{"monto":200,"Tipo_de_pago":"credito","fecha_de_pago":new Date(),"idEmpleado": idMatias},

{"monto":600,"Tipo_de_pago":"contado","fecha_de_pago":new Date(),"idEmpleado": idCarlos},

{"monto":50,"Tipo_de_pago":"credito","fecha_de_pago":new Date(),"idEmpleado": idAngel},

{"monto":500,"Tipo_de_pago":"contado","fecha_de_pago":new Date(),"idEmpleado": idDomingo},

])



db.getCollection("cliente").createIndex({"ci":1})

db.getCollection("cliente").createIndex({"apellido":1})

db.getCollection("cliente").getIndexes()



db.getCollection("empleado").createIndex({"ci":1})

db.getCollection("empleado").createIndex({"Fecha_de_insercion":1})

db.getCollection("empleado").getIndexes()



db.getCollection("heladeria").createIndex({"NIT":1})

db.getCollection("heladeria").createIndex({"nombre":1})

db.getCollection("heladeria").getIndexes()



db.getCollection("pago").createIndex({"monto":1})

db.getCollection("pago").createIndex({"fecha_de_pago":1})

db.getCollection("pago").getIndexes()



db.getCollection("cliente").find()

db.getCollection("heladeria").find()

db.getCollection("pago").find()

db.getCollection("empleado").find()



db.getCollection("cliente").createIndex({"correoCliente":1},{unique:true})

db.getCollection("cliente").getIndexes()



db.getCollection("empleado").createIndex({"correo":1},{unique:true})

db.getCollection("empleado").getIndexes()